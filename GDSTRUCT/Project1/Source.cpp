#include <iostream>
#include <string>
#include <time.h>

using namespace std;

void RAND(int INT[])
{
	cout << "Generated Array:\t";
	for (int i = 0; i<10; i++)
	{
		INT[i] = rand() % 69 + 1;
		cout << INT[i] << ' ';
	}
	cout << endl;
}

void ASC(int INT[])
{
	int temp;

	cout << "Ascending Array:\t";
	for (int x = 0; x<10; x++)
	{
		for (int y = x + 1; y<10; y++)
		{
			if (INT[x] > INT[y])
			{
				temp = INT[x];
				INT[x] = INT[y];
				INT[y] = temp;
			}
		}
		cout << INT[x] << ' ';
	}
	cout << endl;
}

void DSC(int INT[])
{
	int temp;

	cout << "Descendign Array:\t";
	for (int x = 0; x<10; x++)
	{
		for (int y = x + 1; y<10; y++)
		{
			if (INT[x] < INT[y])
			{
				temp = INT[x];
				INT[x] = INT[y];
				INT[y] = temp;
			}
		}
		cout << INT[x] << ' ';
	}
	cout << endl;
}

void LSearch(int INT[])
{
	bool Found = false;
	int Number;

	cout << "Number to Locate: ";
	cin >> Number;
	for (int i = 0; i < 10; i++)
	{
		if (Number == INT[i])
		{
			cout << "Number " << Number << " located in index " << i << "\n\n" << endl;
			Found = true;
		}
	}
	if (Found == false)
	{
		cout << "Number " << Number << " was not located in the array.\n\n" << endl;
	}
}

int main()
{
	srand(time(NULL));
	int Numbers[10];
	char Sort;

	//Generated randomized array
	RAND(Numbers);

	//shows user options for arranging the arrays
	cout << "Arrange the array by Ascending or Descending. (A\D) \n";

	//input
	do
	{
		cout << "Enter 'A' or 'D' only: ";
		cin >> Sort;
	} while ((Sort != 'A') && (Sort != 'D'));

	system("cls");

	//Will use the functions for ASC or DSC
	if (Sort == 'A') ASC(Numbers);
	else if (Sort == 'D') DSC(Numbers);

	//Allows the user to search number in array
	LSearch(Numbers);

	system("pause");
	return 0;
}